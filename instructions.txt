Mini Activity:

Create a new expressjs api in a folder called booking-system-api.

You can follow our step by step in the previous repo.

Create 2 new routes with the same endpoint /courses

-get method route to show a message:
	"This route will get all course document"

-post method route to show a message:
	"This route will create a new course document"

Run your server with gitbash/terminal and send a screenshot of at least one of the responses from postman
