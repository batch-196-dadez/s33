/*
	TO be able to create routes from another file to be used in our application, from here, we have to import express as well, however, we will now use another method from express to contain our routes.

	the Router() method,will us to contain our routes
*/

const express = require("express");
const router = express.Router();

//Import the Course model so we can manipulate it and add a new course document.
const Course = require("../models/Course");

//All routes to courses now has an endpoint prefaced with /courses
//endpoint - /courses/
router.get('/',(req,res)=>{

	// Use the Course model to connect to our collection and retrieve our courses.
	// To be able to query into our collections we use the model connected to that collection
	// in mongoDB: db.courses.find
	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

});

//endpoint /courses/
router.post('/',(req,res)=>{

	//console.log(req.body);
	

	//Using the Course model, we will use its constructor to create our Course document which will follow the schema of the model, and add methods for document creation.

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})
	// console.log(newCourse)

	// newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor
	// .save() method is added into our newCourse object. This will allow us to save the content of newCourse into our collection
	//equivalent to db.courses.insertOne()
	newCourse.save()

	// .then() allows us to process the result of a previous function/method in its own anonymous function
	.then(result => res.send(result))

	// .catch() - catches the errors and allows us to prcess and send to client
	.catch(error => res.send("Invalid input!"))

});

module.exports = router;
