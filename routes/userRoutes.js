const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
// bcrypt is a package which allows us to has our passwords to add a layer of security for our users' details
const User = require("../models/User");

//register
router.post('/',(req,res)=>{

	//console.log(req.body);
	

	//Using the Course model, we will use its constructor to create our Course document which will follow the schema of the model, and add methods for document creation.

    // Using bcrypt, we're going to hide the user's password underneath a layer of randomized characters. Salt rounds are the number of times we randomize the string/password hash

    // bcrypt.hashSync(<string>,<saltRounds>)
    const hashedPw = bcrypt.hashSync(req.body.password,10)
    
	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo

	})
	// console.log(newCourse)

	// newCourse is now an object which follows the courseSchema and with additional methods from our Course constructor
	// .save() method is added into our newCourse object. This will allow us to save the content of newCourse into our collection
	//equivalent to db.courses.insertOne()
	newUser.save()

	// .then() allows us to process the result of a previous function/method in its own anonymous function
	.then(result => res.send(result))

	// .catch() - catches the errors and allows us to prcess and send to client
	.catch(error => res.send("Invalid input!"))

});


router.post('/details',(req,res)=>{

	console.log(req.body)
    User.find({"_id":req.body.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))

});


module.exports = router;
